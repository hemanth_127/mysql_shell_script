#!/bin/bash

USER="root"
PASSWORD="password"
HOST="localhost"


DATABASE="project1"

read -p "Table :" TABLE

SQL="CREATE TABLE IF NOT EXISTS $TABLE (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);"

mysql -u $USER -p$PASSWORD -h $HOST $DATABASE -e "$SQL"

if [ $? -eq 0 ]; then
    echo "Table '$TABLE' created successfully in database '$DATABASE'!"
else
    echo "Failed to create table '$TABLE' in database '$DATABASE'."
fi
