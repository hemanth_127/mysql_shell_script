#!/bin/bash

USER="root"
PASSWORD="password"
HOST="localhost"
DATABASE="project1"
csv_file="./matches.csv"

table="matches"

headers=$(head -n 1 $csv_file)

create_table="create table if not exists $table ("
for header in $(echo $headers | sed "s/,/ /g"); do
    create_table+="$header VARCHAR(255),"
done

create_table=${create_table%,}
create_table+=");"

mysql -u $USER -p$PASSWORD -h $HOST $DATABASE -e "$create_table"

if [ $? -eq 0 ]; then
    echo "table '$table' created successfully!"
else
    echo "Failed to create table '$table'."
    exit 1
fi

LOAD_DATA_SQL="
LOAD DATA LOCAL INFILE '$csv_file'
INTO table $table
FIELDS TERMINATED BY ','
ENCLOSED BY '\"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
"

mysql --local-infile=1 -u $USER -p$PASSWORD -h $HOST $DATABASE -e "$LOAD_DATA_SQL"

if [ $? -eq 0 ]; then
    echo "CSV data loaded successfully into table '$table'!"
else
    echo "Failed to load CSV data into table '$table'."
fi
