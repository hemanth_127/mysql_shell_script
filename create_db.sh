#!/bin/bash

USER="root"
PASSWORD="password"
HOST="localhost"

read DATABASE

SQL="CREATE DATABASE IF NOT EXISTS $DATABASE;"

mysql -u $USER -p$PASSWORD -h $HOST -e "$SQL"

if [ $? -eq 0 ]; then
    echo "Database '$DATABASE' created successfully!"
else
    echo "Failed to create database '$DATABASE'."
fi
