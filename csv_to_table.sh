#!/bin/bash

USER="root"
PASSWORD="password"
HOST="localhost"
DATABASE="project1"
CSV_FILE="./deliveries.csv"
TABLE="deliveries"

HEADERS=$(head -n 1 "$CSV_FILE")


CREATE_TABLE="CREATE TABLE IF NOT EXISTS $TABLE ("
for HEADER in $(echo $HEADERS | sed "s/,/ /g"); do
    COLUMN=$(echo $HEADER | sed 's/[^a-zA-Z0-9_]/_/g')
    CREATE_TABLE+="\`$COLUMN\` VARCHAR(255),"
done
CREATE_TABLE=${CREATE_TABLE%,}
CREATE_TABLE+=");"


mysql --local-infile=1 -u"$USER" -p"$PASSWORD" -h "$HOST" "$DATABASE" -e "$CREATE_TABLE"

if [ $? -eq 0 ]; then
    echo "Table '$TABLE' created successfully!"
else
    echo "Failed to create table '$TABLE'."
    exit 1
fi


LOAD_DATA_SQL="
LOAD DATA LOCAL INFILE '$CSV_FILE'
INTO TABLE $TABLE
FIELDS TERMINATED BY ',' 
ENCLOSED BY '\"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
"

mysql --local-infile=1 -u"$USER" -p"$PASSWORD" -h "$HOST" "$DATABASE" -e "$LOAD_DATA_SQL"

if [ $? -eq 0 ]; then
    echo "CSV data loaded successfully into table '$TABLE'!"
else
    echo "Failed to load CSV data into table '$TABLE'."
fi

